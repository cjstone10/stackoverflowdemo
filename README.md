TODO List
- use dagger for dependency injection;
- use fragment navigation instead of activity navigation;
- handle no internet connection cases;
- handle error and empty data cases;